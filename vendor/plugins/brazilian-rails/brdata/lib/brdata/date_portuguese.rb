class Date
  
  # Retorna a data no padrao brasileiro
  #
  # Exemplo:
  #  data = Date.new(2007, 9, 27)
  #  data.to_s_br ==> "27/09/2007"
  def to_s_br
    strftime("%d/%m/%Y")
  end
  
  # Valida se uma string eh uma data valida
  #
  # Exemplo:
  #  Date.valid?('01/01/2007') ==> true
  #  Date.valid?('32/01/2007') ==> false
  def self.valid?(date)
    begin
      date = date.to_date
      Date.valid_civil?(date.year, date.month, date.day)        
    rescue
      return false
    end
    true
  end
  
end
