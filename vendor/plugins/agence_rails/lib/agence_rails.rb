# AgenceRails
module AgenceRails
  PLUGIN_NOME = 'agence_rails'
  PLUGIN_CAMINHO = "#{RAILS_ROOT}/vendor/plugins/#{PLUGIN_NOME}"
  PLUGIN_CAMINHO_PUBLICO = "#{PLUGIN_CAMINHO}/public"
  PLUGIN_CAMINHO_CONTROLLERS = "#{PLUGIN_CAMINHO}/app/controllers"  
  PLUGIN_CAMINHO_VIEWS = "#{PLUGIN_CAMINHO}/app/views"  
end