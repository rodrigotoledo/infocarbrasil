module ActionPackCalendario
  def field_calendar(modelo, atributo,opcoes = {},opcoes_js = {})
    
    opcoes_padrao     = {:maxlength => 10,
                         :size => 10,
                         :style => "width:auto",
                         :readonly => "readonly"}

    opcoes = opcoes_padrao.merge opcoes
    
    opcoes_padrao_js  = {:showsTime => false,
                         :ifFormat => "%d/%m/%Y",
                         :inputField => "#{modelo}_#{atributo}",
                         :button => "bt_#{modelo}_#{atributo}",
                         :align => "Bl"}
                         
    opcoes_js = opcoes_padrao_js.merge opcoes_js
    
    html = text_field(modelo.to_sym,atributo.to_sym, opcoes)
    html << calendar_button(opcoes_padrao_js[:button])
    html << calendar_js(opcoes_js)
    html
    
  end
  
  
  def field_calendar_tag(nome,valor,opcoes = {},opcoes_js = {})
    opcoes_padrao     = {:maxlength => 10,
                         :size => 10,
                         :style => "width:auto",
                         :readonly => "readonly"}

    opcoes = opcoes_padrao.merge opcoes
    
    opcoes_padrao_js  = {:showsTime => false,
                         :ifFormat => "%d/%m/%Y",
                         :inputField => "#{nome}",
                         :button => "bt_#{nome}",
                         :align => "Bl"}
                         
    opcoes_js = opcoes_padrao_js.merge opcoes_js
    
    html = text_field_tag(nome,valor, opcoes) 
    html << calendar_button(opcoes_padrao_js[:button])
    html << calendar_js(opcoes_js)
    html
    
  end
  
  
  
  def calendar_js(opcoes = {})
    opcoes_s =  opcoes.map do |k, v| 
                  if (!v.is_a?(FalseClass) && !v.is_a?(TrueClass))
                    %(#{k}:"#{v}")
                  else
                    %(#{k}:#{v})
                  end
                end
    
    opcoes_s = opcoes_s.join(",\n")
    
    html =  '<script type="text/javascript">'
    html << "Calendar.setup({"
    html << opcoes_s
    html << "});"
    html << "</script>"
    html
  end
    
  def calendar_button(id)
    " " + image_tag("icons/date.gif", :class => "dateIco", :id => id)
  end
end


ActionView::Base.send(:include, ActionPackCalendario)