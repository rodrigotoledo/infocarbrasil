module ActionView

  module Helpers
    
    module PrototypeHelper
      protected
      def build_callbacks(options)
        callbacks = {}
        options.each do |callback, code|
          if CALLBACKS.include?(callback)
            name = 'on' + callback.to_s.capitalize
            if callback.to_s.capitalize == 'Complete'
              callbacks[name] = "function(request){linesTrue();ocultarLoading();#{code}}"
            elsif callback.to_s.capitalize == 'Complete'
              callbacks[name] = "function(request){exibirLoading();#{code}}"
            else
              callbacks[name] = "function(request){#{code}}"
            end
            
          end
        end
        
        if callbacks['onComplete'].nil?
          callbacks['onComplete'] = "function(request){linesTrue();ocultarLoading();}"
        end
        
        if callbacks['onLoading'].nil?
          callbacks['onLoading'] = "function(request){exibirLoading()}"
        end
        
        
        callbacks
      end

    end
    
  end
  
end