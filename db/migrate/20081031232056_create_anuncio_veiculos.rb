class CreateAnuncioVeiculos < ActiveRecord::Migration
  def self.up
    create_table :anuncio_veiculos do |t|
      t.references :anuncio
      t.references :modelo
      t.references :marca
      t.references :tipo_veiculo
      t.references :cor
      t.references :pais
      t.references :estado
      t.references :cidade
      t.integer :ano
      t.decimal :valor
      t.integer :combustivel
      t.integer :kilometragem

      t.timestamps
    end
  end

  def self.down
    drop_table :anuncio_veiculos
  end
end
