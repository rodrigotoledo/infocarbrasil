class CreateAnuncioBanners < ActiveRecord::Migration
  def self.up
    create_table :anuncio_banners do |t|
      t.references :anuncio
      t.text :codigo_flash
      t.string :arquivo
      t.integer :tipo

      t.timestamps
    end
  end

  def self.down
    drop_table :anuncio_banners
  end
end
