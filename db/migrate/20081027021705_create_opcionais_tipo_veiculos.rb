class CreateOpcionaisTipoVeiculos < ActiveRecord::Migration
  def self.up
    create_table :opcionais_tipo_veiculos, :id => false do |t|
      t.references :opcional
      t.references :tipo_veiculo
    end
  end

  def self.down
    drop_table :opcionais_tipo_veiculos
  end
end
