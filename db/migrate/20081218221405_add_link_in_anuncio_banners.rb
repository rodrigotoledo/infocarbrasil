class AddLinkInAnuncioBanners < ActiveRecord::Migration
  def self.up
    change_table :anuncio_banners do |t|
      t.string :link
    end
  end

  def self.down
    change_table :anuncio_banners do |t|
      t.remove :link
    end
  end
end
