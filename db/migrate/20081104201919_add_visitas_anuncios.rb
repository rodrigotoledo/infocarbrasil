class AddVisitasAnuncios < ActiveRecord::Migration
  def self.up
    change_table(:anuncios) do |t|
      t.integer :visitas, :default => 0
    end
  end

  def self.down
    change_table(:anuncios) do |t|
      t.remove :visitas
    end
  end
end
