class CreateNoticias < ActiveRecord::Migration
  def self.up
    create_table :noticias do |t|
      t.references :topico_noticia
      t.string :titulo
      t.string :autor
      t.string :fonte
      t.string :entrada
      t.text :texto
      t.boolean :ativo

      t.timestamps
    end
  end

  def self.down
    drop_table :noticias
  end
end
