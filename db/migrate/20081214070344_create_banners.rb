class CreateBanners < ActiveRecord::Migration
  def self.up
    create_table :banners do |t|
      t.integer :tipo
      t.integer :localizacao
      t.string :arquivo
      t.string :link
      t.boolean :ativo

      t.timestamps
    end
  end

  def self.down
    drop_table :banners
  end
end
