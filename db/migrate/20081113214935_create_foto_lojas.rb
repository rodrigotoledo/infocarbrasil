class CreateFotoLojas < ActiveRecord::Migration
  def self.up
    create_table :foto_lojas do |t|
      t.references :loja
      t.string :arquivo

      t.timestamps
    end
  end

  def self.down
    drop_table :foto_lojas
  end
end
