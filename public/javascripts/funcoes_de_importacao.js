/**
 * @author rodrigo.toledo@agence.com.br
 */
var timeout;
var enviar = false;
var importacao_atual = 0;
function iniciar_importacao()
{
	enviar = true;
	importar_linha();
	$('btn_iniciar').hide();
	$('btn_parar').show();
}

function parar_importacao()
{
	enviar = false;
	$('btn_iniciar').show();
	$('btn_parar').hide();
}