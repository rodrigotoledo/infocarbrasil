module ContatoHelper
  def estados 
    opcoes = [["Selecione -->",nil]]
    opcoes += Estado.all(:select => "id,nome", :conditions => ["pais_id = ?",params[:contato][:pais_id].to_i], :order => "nome").collect{|t| [t.nome,t.id]}
  rescue
    opcoes = [["Selecione -->",nil]]
  end
  
  def cidades 
    opcoes = [["Selecione -->",nil]]
    opcoes += Cidade.all(:select => "id,nome", :conditions => ["estado_id = ?",params[:contato][:estado_id].to_i], :order => "nome").collect{|t| [t.nome,t.id]}
  rescue
    opcoes = [["Selecione -->",nil]]
  end
  
end
