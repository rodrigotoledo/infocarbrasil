module Admin::AnunciantesHelper
  
  def topo_cabecalho
    Anunciante::TIPOS[session[:tipo_anunciante]].pluralize
  end
  
  
  def anunciante_loja?
    session[:tipo_anunciante] == Anunciante::LOJA
  end
  
  def label_nome
    anunciante_loja? ? "Nome Fantasia" : "Nome"
  end
  
  def tipo_lojas
    TipoLoja.all(:order => "descricao")
  end  
  
  def estados
    params[:estado_id] ||= @anunciante.cidade.estado_id rescue nil
    
    opcoes = [["Selecione -->",nil]]
    opcoes += Estado.all(:order => "nome").collect{|t| [t.nome,t.id]}
    options_for_select(opcoes,params[:estado_id].to_i)
  end
  
  
  def cidades
    opcoes = []
    opcoes += Cidade.all(:conditions => ["estado_id = ?",params[:estado_id].to_i], :order => "nome").collect{|t| [t.nome,t.id]}
    opcoes
  end
end
