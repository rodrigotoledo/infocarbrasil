module PainelAnuncianteHelper

  def acoes(anuncio)
    if anuncio.status
      link_to_remote image_tag("icon_delete.jpg"), :url => {:action => :expirar, :id => anuncio}
    elsif !anuncio.status.nil? && anuncio.pode_reativar?
      link_to_remote image_tag("icon_edit.jpg"), :url => {:action => :reativar, :id => anuncio}
    elsif !anuncio.pagamento_efetuado?
      efetuar_pagamento(anuncio)
    end
  end
  
  def efetuar_pagamento(anuncio)
    button_to "Boleto", :action => :gerar_boleto, :id => anuncio
  end

  def status_anuncio(anuncio)
    if anuncio.status
      "Ativo"
    elsif !anuncio.status && anuncio.expira_em >= Date.today
      "Inativo"
    else
      "Expirado"
    end
  end
  
  def situacao_financeira(anuncio)
    anuncio.pagamento_efetuado? ? "Pago" : "Pendente de Pagamento"
  end
  
  def tipos_planos
    opcoes = [["Indiferente",nil]]
    opcoes += Plano::TIPOS.collect{|i,valor| [valor,i]}
    opcoes
  end
  
  def get_tipo(tipo)
    Plano::TIPOS[tipo]
  end

  def status
    opcoes = [["Indiferente",nil]]
    opcoes += Anuncio::STATUS.collect{|i,valor| [valor,i]}
    opcoes
  end
  
  
  def descricao_pedido(pedido)
    [pedido.interessado,pedido.email,pedido.telefone,pedido.celular].join ", "
  end
  
  def submenu
    itens = []
    itens << ["Relatórios",{:action => :index}]
    itens << ["Alterar dados pessoais",{:controller => :cadastro, :action => :editar}]
    itens << ["Interesses de Compra",{:action => :interesses}]
    itens << ["Sair",{:controller => :login, :action => :deslogar}]
    links = itens.collect{|texto,caminho| content_tag(:li,link_to(texto, caminho))}
    content_tag :ul, links.join(content_tag(:li,"  |  "))
  end
  
end