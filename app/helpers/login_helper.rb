module LoginHelper
  
  def tipos_login(formulario)
    tipos = []
    Anunciante::TIPOS.each do |tipo,descricao|
      tipos << formulario.radio_button(:tipo_login,tipo,:class => "input") + descricao
    end
    tipos.join "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"
  end
end
