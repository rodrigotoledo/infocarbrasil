class Contato < ActiveRecord::BaseWithoutTable
  column :nome, :string
  column :email, :string
  column :mensagem, :text
  column :pais_id, :integer  
  column :estado_id, :integer
  column :cidade_id, :integer
  column :telefone, :string
  column :celular, :string
  
  belongs_to :pais
  belongs_to :estado
  belongs_to :cidade
    
  attr_accessor :corpo
  validates_presence_of :nome, :email, :mensagem

  def enviar_email
    if !self.valid?
      return false
    end
    
    Notificador.deliver_enviar_email_contato(self)
  end  

end
