class Loja < ActiveRecord::Base
  
  belongs_to :anunciante
  has_many :foto_lojas, :class_name => "FotoLoja", :dependent => :destroy
  has_one :imagem_principal, :class_name => "FotoLoja"
  
  validates_presence_of :site, :cnpj, :telefone_responsavel, :celular_responsavel, :razao_social
  validates_uniqueness_of :cnpj
  
  usar_como_cnpj(:cnpj)
  
  
  has_and_belongs_to_many :tipo_lojas, :join_table => "lojas_tipo_lojas"
  
  
  
  def self.buscar(pagina)
    Loja.paginate :joins => "INNER JOIN anunciantes as a ON lojas.anunciante_id = a.id",
                  :conditions => ["a.ativo = ?",true],
                  :per_page => 1, :page => pagina,
                  :order => "nome"
  end
  
  
  def anuncios(pagina)
    self.anunciante.anuncios.paginate :per_page => MAXIMO_POR_PAGINA,
                                      :page => pagina,
                                      :order => "anuncios.expira_em desc"
  end
end
