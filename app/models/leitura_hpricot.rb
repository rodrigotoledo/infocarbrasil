require 'hpricot'
require 'open-uri'

class LeituraHpricot < ActiveRecord::BaseWithoutTable
  def ids_site
    ids_site = {}
    
    ids_site[127]="AMAZONAS"
    ids_site[63]="BMW"
    ids_site[126]="BUELL"
    ids_site[172]="BY CRISTO Triciclos"
    ids_site[84]="CAGIVA"
    ids_site[152]="DAFRA"
    ids_site[173]="DAYUN"
    ids_site[69]="DUCATI"
    ids_site[170]="FYM"
    ids_site[184]="GMM Quadriciclos"
    ids_site[158]="HAOBAO"
    ids_site[42]="HARLEY DAVIDSON"
    ids_site[182]="HOADB Quadriciclos"
    ids_site[40]="HONDA"
    ids_site[159]="HUSQVARNA"
    ids_site[60]="KASINSKI"
    ids_site[41]="KAWASAKI"
    ids_site[181]="KENTON"
    ids_site[183]="MBW Quadriciclos"
    ids_site[151]="MIZA"
    ids_site[161]="MVK"
    ids_site[160]="PEUGEOT"
    ids_site[59]="SUNDOWN"
    ids_site[39]="SUZUKI"
    ids_site[132]="TRAXX"
    ids_site[111]="TRICERON Triciclos"
    ids_site[58]="TRIUMPH"
    ids_site[131]="VESPA"
    ids_site[163]="VMB"
    ids_site[174]="X-TRIKE Triciclos"
    ids_site[38]="YAMAHA"
    ids_site[56]="OUTRAS"
    
    ids_site[133]="AGRALE"
    ids_site[33]="ALFA ROMEO"
    ids_site[99]="AM GENERAL"
    ids_site[142]="ASIA MOTORS"
    ids_site[120]="ASTON MARTIN"
    ids_site[1]="AUDI"
    ids_site[94]="BENTLEY"
    ids_site[2]="BMW"
    ids_site[148]="BRM (Buggy)"
    ids_site[149]="BUGGY"
    ids_site[143]="BUGRE"
    ids_site[102]="CADILLAC"
    ids_site[104]="CBT"
    ids_site[168]="CHAMONIX"
    ids_site[144]="CHANA"
    ids_site[14]="CHRYSLER"
    ids_site[15]="CITROËN"
    ids_site[87]="DAEWOO"
    ids_site[140]="DAIHATSU"
    ids_site[107]="DKW-Vemag"
    ids_site[17]="DODGE"
    ids_site[91]="ENGESA"
    ids_site[145]="ENVEMO"
    ids_site[162]="FARUS"
    ids_site[122]="FERRARI"
    ids_site[8]="FIAT"
    ids_site[146]="FIBRAVAN (Buggy)"
    ids_site[5]="FORD"
    ids_site[6]="GM - Chevrolet"
    ids_site[57]="GMC"
    ids_site[37]="GURGEL"
    ids_site[18]="HONDA"
    ids_site[19]="HYUNDAI"
    ids_site[95]="INFINITI"
    ids_site[119]="JAGUAR"
    ids_site[13]="JEEP"
    ids_site[125]="JPX"
    ids_site[21]="KIA"
    ids_site[35]="LADA"
    ids_site[96]="LAMBORGHINI"
    ids_site[22]="LANDROVER"
    ids_site[103]="LEXUS"
    ids_site[121]="LINCOLN"
    ids_site[180]="MAHINDRA"
    ids_site[124]="MASERATI"
    ids_site[11]="MAZDA"
    ids_site[9]="MERCEDES-BENZ"
    ids_site[97]="MINI"
    ids_site[10]="MITSUBISHI"
    ids_site[171]="MIURA"
    ids_site[134]="MP"
    ids_site[24]="NISSAN"
    ids_site[25]="PEUGEOT"
    ids_site[112]="PONTIAC"
    ids_site[26]="PORSCHE"
    ids_site[64]="PUMA"
    ids_site[27]="RENAULT"
    ids_site[123]="SATURN"
    ids_site[28]="SEAT"
    ids_site[139]="SHELBY"
    ids_site[98]="SMART"
    ids_site[141]="SSANGYONG"
    ids_site[29]="SUBARU"
    ids_site[30]="SUZUKI"
    ids_site[12]="TOYOTA"
    ids_site[32]="TROLLER"
    ids_site[31]="VOLVO"
    ids_site[3]="VW - Volkswagen"
    ids_site[147]="WALK (Buggy)"
    ids_site[175]="WILLYS OVERLAND"
    
    ids_site[43]="AGRALE"
    ids_site[135]="BUSSCAR"
    ids_site[155]="CICCOBUS"
    ids_site[88]="DODGE"
    ids_site[44]="FIAT"
    ids_site[45]="FORD"
    ids_site[90]="GM - Chevrolet"
    ids_site[46]="GMC"
    ids_site[86]="IVECO"
    ids_site[93]="MARCOPOLO"
    ids_site[48]="MERCEDES-BENZ"
    ids_site[156]="NAVISTAR"
    ids_site[157]="NEOBUS"
    ids_site[153]="PUMA"
    ids_site[49]="SCANIA"
    ids_site[50]="VOLVO"
    ids_site[51]="VW"
    ids_site[154]="WALKBUS"
    ids_site[65]="Outras"
    
    
    ids_site[52]="EVINRUDE"
    ids_site[53]="KAWASAKI"
    ids_site[62]="MERCURY"
    ids_site[55]="SEADOO"
    ids_site[169]="SUZUKI"
    ids_site[54]="YAMAHA"
    ids_site[68]="OUTRAS"
    ids_site
  end



	CODIGO_FIXTURE = "
%d:
  marca_id: %d
  descricao: \"%s\""

	def initialize
		# load the RedHanded home page
		fixtures = []
		ids_site.collect do |id,descricao|
      puts "Importando Marca: #{descricao}"
      puts "\t\tElementos: "
			marca = Marca.find_by_descricao(descricao)
	
			doc = Hpricot(open("http://www.shopcar.com.br/busca_avancada.htm/busca_avancada.htm?tipo=1&marca=#{id}"))
			# change the CSS class on links
			elementos = doc.search("//select[@name='modelo']").search("//option[@value!='']")
			elementos.collect.each do |elemento|
				codigo_html = elemento.inner_html
        puts "\t\t\t#{codigo_html}"
				fixtures << sprintf(CODIGO_FIXTURE,rand(rand(Time.now.to_i)+rand(id)),marca.id,codigo_html)
			end
		end
		
		create_file(fixtures.join("\n"))
	end
	
	
	def create_file(str_file)
		File.open(RAILS_ROOT+"/fixtures.yml", 'wb') do |arquivo|
		  arquivo.write str_file
		  arquivo.close    
		end
	end
end
