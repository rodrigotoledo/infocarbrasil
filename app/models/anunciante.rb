class Anunciante < ActiveRecord::Base
  
  has_one :loja
  belongs_to :cidade
  has_many :anuncios
  has_many :anuncios_veiculos, :class_name => "Anuncio"
  
  ANUNCIANTE = 0
  LOJA = 1
  
  TIPOS = {ANUNCIANTE => "Particular",LOJA => "Loja"}
  
  attr_accessor :validar_senha
  attr_accessor :tipo_login, :login
  
  
  validates_presence_of :nome,:cpf,:email,:endereco,:cidade_id,:telefone,:celular,:email_secundario,:tipo
  validates_presence_of :senha, :if => Proc.new{|t| t.validar_senha}
  validates_confirmation_of :senha, :if => Proc.new{|t| !t.senha.blank?}
  validates_uniqueness_of :email,:cpf
  
  usar_como_cpf(:cpf)
  
  
  after_validation :encriptar_senha
  
  def encriptar_senha
    self.senha = Digest::MD5.hexdigest(self.senha) if self.senha_changed?
  end

  def pesquisar_anuncios_veiculos
    anuncios = []
    self.anuncios.collect{|anuncio| anuncios << anuncio if(anuncio.tipo == "AnuncioVeiculo") }
    anuncios
  end  
  
  def self.pesquisa_admin(parametros,pagina,tipo)
    busca = "%#{parametros[:nome]}%"
    self.paginate :conditions => ["tipo = ? AND (nome like ? OR email like ? OR endereco like ?)",tipo,busca,busca,busca], :order => "nome", :per_page => MAXIMO_POR_PAGINA, :page => pagina
  end
  
  def self.pesquisa_anuncios(parametros,pagina)
    
    conditions = "anuncios.descricao like '%#{parametros[:descricao]}%'"

    if parametros[:tipo] && !parametros[:tipo].empty?
      conditions << " and anuncios.tipo = '" +  parametros[:tipo] + "'" 
    end
  
    if parametros[:status] && !parametros[:status].empty?
      conditions << " and anuncios.status is " +  parametros[:status] 
    end
      
    if parametros[:anunciante] && !parametros[:anunciante].empty?
      conditions << " and (anunciantes.nome like '%#{parametros[:anunciante]}%'"      
      conditions << " or anunciantes.cpf = '" + parametros[:anunciante] + "'"
      conditions << " or lojas.cnpj = '" + parametros[:anunciante] + "')"
    end
    
    self.paginate :conditions => conditions,
                  :include => [:anuncios, :loja],
                  :order => "anunciantes.nome, anuncios.descricao" ,
                  :per_page => MAXIMO_POR_PAGINA, :page => pagina
  end
  
  
  def login?
    
    self.senha = Digest::MD5.hexdigest(self.senha) unless self.senha.blank?
    if self.tipo_login.to_i == ANUNCIANTE
      anunciante = Anunciante.find(:first, :conditions => ["(email = ? or cpf = ?) and senha = ? AND ativo = ? AND tipo = ?",self.login,self.login,self.senha,true,ANUNCIANTE])
    else
      anunciante = Anunciante.find(:first, :joins => "INNER JOIN lojas ON anunciantes.id = lojas.anunciante_id",
                 :select => "anunciantes.*",
                 :conditions => ["(email = ? or cnpj = ?) and senha = ? AND ativo = ? AND tipo = ?",self.login,self.login,self.senha,true,LOJA])
    end
    
    if anunciante
      self.id = anunciante.id
      anunciante
    else
      false
    end
    
  end
  
  
  def salvar(tipo,parametros_loja)
    if tipo == ANUNCIANTE
      salvar_anunciante
    else
      salvar_loja(parametros_loja)
    end
  end
  
  def salvar_anunciante
    self.tipo = ANUNCIANTE
    self.save
  end
  
  
  def salvar_loja(parametros_loja = {})
    parametros_loja ||= {}
    self.tipo = LOJA
    self.loja ||= Loja.new
    self.loja.attributes = parametros_loja
    
    Loja.transaction do 
      self.save!
      
      self.loja.anunciante_id = self.id
      self.loja.save!
    end
    true
  rescue
    false
  end
  
  
  
  def endereco_completo
    "#{self.endereco}, #{self.cidade.nome}/#{self.cidade.estado.sigla}"
  end
  
  def loja?
    self.tipo == LOJA
  end
  
  def anunciante?
    self.tipo != LOJA
  end
  
end
