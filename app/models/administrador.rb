class Administrador < ActiveRecord::Base
  
  def login?
    self.senha = Digest::MD5.hexdigest(self.senha) unless self.senha.blank?
    administrador = Administrador.find(:first, :conditions => ["email =? and senha = ?",self.email,self.senha])
    
    if administrador
      self.id = administrador.id
      administrador
    else
      false
    end
  end
end
