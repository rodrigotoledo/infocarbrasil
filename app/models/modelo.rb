class Modelo < ActiveRecord::Base
  belongs_to :marca
  
  validates_presence_of :descricao, :marca_id
  
  def self.pesquisa_admin(parametros,pagina)
    busca = "%#{parametros[:descricao]}%"
    self.paginate :conditions => ["marcas.descricao like ? OR modelos.descricao like ?",busca,busca],
                  :include => :marca,
                  :order => "modelos.descricao,marcas.descricao", :per_page => MAXIMO_POR_PAGINA, :page => pagina
  end
end
