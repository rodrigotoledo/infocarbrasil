class NoticiasController < FrontendController
  def index
    params[:page] = 1 if params[:page].to_i <= 0
    @noticia_destaque = Noticia.first(:order => "created_at DESC")
    @noticias = Noticia.paginate :order => "created_at DESC", :per_page => MAXIMO_POR_PAGINA, :page => params[:page]
  end

  def por_categoria
    params[:page] = 1 if params[:page].to_i <= 0
    
    @topico_noticia = TopicoNoticia.find(params[:id])
    @noticias = @topico_noticia.buscar_noticias_paginadas(params[:page])
    @noticia_destaque = @topico_noticia.noticias.first
    render :action => :index
  rescue
    redirect_to :action => :index
  end

  def ver
    @noticia = Noticia.find_by_id_and_ativo(params[:id], true)
  rescue
    redirect_to :action => :index
  end

end
