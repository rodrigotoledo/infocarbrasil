class HomeController < FrontendController
  def index
    @noticias = Noticia.all(:limit => 4, :include => :foto_principal, :order => "created_at DESC")
    
    @anuncios_veiculos_destaques = Anuncio.all(:limit => 6,:order => "rand()",
                                   :include => [:plano, :anuncio_veiculo],
                                   :conditions => ["status = ? AND planos.destaque = ? AND anuncios.tipo = ?",true,true,Plano::TIPO_VEICULO])

    @anuncios_acessorios_destaques = Anuncio.all(:limit => 4,:order => "rand()",
                                   :include => [:plano, :anuncio_acessorio],
                                   :conditions => ["status = ? AND planos.destaque = ? AND anuncios.tipo = ?",true,true,Plano::TIPO_ACESSORIO])
                                   
    @anuncios_veiculos_ofertas = Anuncio.all(:limit => 6,:order => "rand()",
                                   :include => [:plano, :anuncio_veiculo],
                                   :conditions => ["status = ? AND anuncios.id NOT IN(?) AND anuncios.tipo = ?",
                                   true,@anuncios_veiculos_destaques.collect{|t| t.id},Plano::TIPO_VEICULO])
  end

end
