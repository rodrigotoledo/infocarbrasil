# Filters added to this controller apply to all controllers in the application.
# Likewise, all the methods added will be available for all controllers.

class ApplicationController < ActionController::Base
  # helper :all # include all helpers, all the time

  # See ActionController::RequestForgeryProtection for details
  # Uncomment the :secret if you're not using the cookie session store
  protect_from_forgery # :secret => 'a7affbd4aaa6dfd34006feca970c6227'

  # See ActionController::Base for details
  # Uncomment this to filter the contents of submitted sensitive data parameters
  # from your application log (in this case, all fields with names like "password").
  filter_parameter_logging :senha

  layout :setar_layout

  def setar_layout
    request.request_uri.include?("admin") ? "application_admin" : "application"
  end

  after_filter :limpar_flash

  def limpar_flash
    [:notice,:warning,:error,:info].collect{|t| flash[t] = nil}
  end

  before_filter :manutencao

  def manutencao
    ManutencaoAnuncio.new
  end
end
