class AnunciarAcessorioController < AnuncioController
  def set_anuncio
    @anuncio                    = Anuncio.new
    @anuncio_acessorio          = @anuncio.anuncio_acessorio
    @anuncio_acessorio          ||= AnuncioAcessorio.new
    
    session[:anuncio_temporario] = nil
  end
  
  
  def index
    set_anuncio
  end

  def criar
    set_anuncio
    
    
    @anuncio.attributes = params[:anuncio]
    @anuncio_acessorio.attributes = params[:anuncio_acessorio]
    
    respond_to do |format|
      if @anuncio.valid? & @anuncio_acessorio.valid?
        @anuncio.save
        @anuncio_acessorio.anuncio = @anuncio
        @anuncio_acessorio.save
        
        salvar_imagens(@anuncio)
        
        session[:anuncio_temporario] = @anuncio.id
        
        format.html { redirect_to :controller => :painel_anunciante }
        format.xml  { render :xml => [@anuncio,@anuncio_acessorio], :status => :created, :location => @anuncio }
      else
        flash[:error] = "Preencha os dados corretamente"
        format.html { render :action => "index" }
        format.xml  { render :xml => [@anuncio.errors,@anuncio_acessorio.errors], :status => :unprocessable_entity }
      end
    end
  end

end
