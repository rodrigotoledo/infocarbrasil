class LojasController < FrontendController
  
  def index
    params[:page] = 1 if params[:page].to_i <= 0
    @lojas = Loja::buscar(params[:page])
  end
  
  def estoque
    params[:page] = 1 if params[:page].to_i <= 0
    
    @loja = Loja.find(params[:id], :include => :anunciante)
    @anuncios = @loja.anuncios(params[:page])
    
  rescue
    redirect_to :action => :index
  end
end
