class AnunciarVeiculoController < AnuncioController
  
  def set_anuncio
    @anuncio                  = Anuncio.new
    @anuncio_veiculo          = @anuncio.anuncio_veiculo
    @anuncio_veiculo          ||= AnuncioVeiculo.new
    
    session[:anuncio_temporario] = nil
  end
  
  
  def index
    set_anuncio
  end

  def criar
    set_anuncio

    @anuncio.attributes = params[:anuncio]
    @anuncio_veiculo.attributes = params[:anuncio_veiculo]
    
    respond_to do |format|
      if @anuncio.valid? & @anuncio_veiculo.valid?

        @anuncio.save
        @anuncio_veiculo.anuncio = @anuncio
        @anuncio_veiculo.save
        
        salvar_imagens(@anuncio)
        
        session[:anuncio_temporario] = @anuncio.id
        
        format.html { redirect_to :controller => :painel_anunciante }
        format.xml  { render :xml => [@anuncio,@anuncio_veiculo], :status => :created, :location => @anuncio }
      else
        flash[:error] = "Preencha os dados corretamente"
        format.html { render :action => "index" }
        format.xml  { render :xml => @anuncio.errors, :status => :unprocessable_entity }
      end
    end
  end


  def buscar_marcas
    respond_to do |formato|
      formato.js
    end
  end
  
  
  def buscar_modelos
    respond_to do |formato|
      formato.js
    end
  end
  
  def buscar_estados
    respond_to do |formato|
      formato.js
    end
  end
  
  def buscar_cidades
    respond_to do |formato|
      formato.js
    end
  end

end
