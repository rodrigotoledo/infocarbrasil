class AcessoriosController < FrontendController
  def ver
    @anuncio = Anuncio.find(params[:id], :include => [{:anunciante => :cidade},:anuncio_acessorio])
    @anuncio.update_attribute(:visitas, @anuncio.visitas.next)
    @envio_proposta = EnvioProposta.new
  rescue
    redirect_to :action => :index
  end
  
  
  def enviar_proposta
    @envio_proposta = EnvioProposta.new
    @envio_proposta.attributes = params[:envio_proposta]
    @envio_proposta.anuncio_id = params[:id]
    
    if @envio_proposta.enviar_email
      @envio = true
      @envio_proposta = EnvioProposta.new
    end
    
    respond_to do |formato|
      formato.html {redirect_to :action => :index}
      formato.js
    end
  end
  
  def index

  end

  def pesquisar
    params[:page] = 1 if params[:page].to_i <= 0
    @busca_anuncio_acessorio.attributes = params[:busca_anuncio_acessorio]
    @anuncios = @busca_anuncio_acessorio.buscar(params[:page])
    
    respond_to do |formato|
      formato.html { render :action => "buscar" }
      formato.js { render :action => "pesquisar.rjs" }
    end
  end

end
