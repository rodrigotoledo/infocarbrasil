class Admin::AnunciantesController < Admin::RestritoController
  
  def index
    session[:tipo_anunciante] = Anunciante::TIPOS.include?(params[:tipo].to_i) ? params[:tipo].to_i : Anunciante::ANUNCIANTE
    pesquisar
  end

  def novo
    @anunciante = Anunciante.new
    
    respond_to do |format|
      format.html # new.html.erb
      format.xml  { render :xml => @anunciante }
    end
  end

  def editar
    @anunciante = Anunciante.find(params[:id])
    @loja       = @anunciante.loja
    
    session[:tipo_anunciante] = @anunciante.tipo
  end

  def criar
    return redirect_to(:action => :index) if acao_cancelar?
    
    @anunciante = Anunciante.new params[:anunciante]
    @anunciante.validar_senha = true

    respond_to do |format|
      if @anunciante.salvar(session[:tipo_anunciante],params[:loja])
        @loja = @anunciante.loja
        if @anunciante.loja
          expire_cache(CACHE_HOME_TIPO_LOJAS)
          expire_cache(CACHE_SITE_BUSCA_LOJAS)
        end
        
        sucesso_criar
        format.html { redirecionamento(@anunciante) }
        format.xml  { render :xml => @anunciante, :status => :created, :location => @anunciante }
      else
        @loja = @anunciante.loja
        
        erro_criar
        format.html { render :action => "novo" }
        format.xml  { render :xml => @anunciante.errors, :status => :unprocessable_entity }
      end
    end
  end

  def atualizar
    return redirect_to(:action => :index) if acao_cancelar?
    
    @anunciante = Anunciante.find(params[:id])
    @anunciante.attributes = params[:anunciante]

    respond_to do |format|
      if @anunciante.salvar(session[:tipo_anunciante],params[:loja])
        if @anunciante.loja
          expire_cache(CACHE_HOME_TIPO_LOJAS)
          expire_cache(CACHE_SITE_BUSCA_LOJAS)
        end
        
        sucesso_atualizar
        format.html { redirecionamento(@anunciante) }
        format.xml  { head :ok }
      else
        
        @loja = @anunciante.loja
        
        erro_atualizar
        format.html { render :action => "editar" }
        format.xml  { render :xml => @anunciante.errors, :status => :unprocessable_entity }
      end
    end
  end
  
  def servicos
    @anunciante = Anunciante.find(params[:id])
    @loja       = @anunciante.loja
    
    session[:tipo_anunciante] = @anunciante.tipo
  end
  
  def atualizar_servicos
    return redirect_to(:action => :index, :tipo => 1) if acao_cancelar?
    @anunciante = Anunciante.find(params[:id])
    
    respond_to do |format|
      @anunciante.salvar_loja(params[:loja])
      
#      if @anunciante.loja
#        expire_cache(CACHE_HOME_TIPO_LOJAS)
#        expire_cache(CACHE_SITE_BUSCA_LOJAS)
#      end
        
      sucesso_atualizar
      format.html { redirect_to :action => :servicos, :id => @anunciante }
      format.xml  { head :ok }
      
    end    
  end
  
  def apagar
    @anunciante = Anunciante.find(params[:id])
    @anunciante.destroy
    expire_cache(CACHE_HOME_TIPO_LOJAS)
    expire_cache(CACHE_SITE_BUSCA_LOJAS)

    pesquisar
  end

  def pesquisar
    params[:page] ||= 1
    params[:anunciante] ||= {}
    @anunciantes = Anunciante::pesquisa_admin(params[:anunciante],params[:page],session[:tipo_anunciante])
    respond_to do |formato|
      formato.html { render :action => "index" }
      formato.js   { render :action => "pesquisar.rjs" }
      formato.xml  { render :xml => @anunciantes }
    end
  end
  
  
  
  def buscar_cidades
    respond_to do |formato|
      formato.js   { render :action => "buscar_cidades.rjs" }
    end
  end
end
