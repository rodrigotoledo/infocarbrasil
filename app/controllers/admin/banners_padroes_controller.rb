class Admin::BannersPadroesController < Admin::RestritoController
  def index
    pesquisar
  end

  def novo
    @banner = Banner.new
    
    respond_to do |format|
      format.html # new.html.erb
      format.xml  { render :xml => @banner }
    end
  end

  def editar
    @banner        = Banner.find(params[:id])
  end

  def criar
    return redirect_to(:action => :index) if acao_cancelar?
    
    @banner = Banner.new params[:banner]

    respond_to do |format|
      if @banner.save
        sucesso_criar
        format.html { redirecionamento(@banner) }
        format.xml  { render :xml => @banner, :status => :created, :location => @banner }
      else
        erro_criar
        format.html { render :action => "novo" }
        format.xml  { render :xml => @banner.errors, :status => :unprocessable_entity }
      end
    end
  end

  def atualizar
    return redirect_to(:action => :index) if acao_cancelar?

    @banner = Banner.find(params[:id])

    respond_to do |format|
      if @banner.update_attributes(params[:banner])
        sucesso_atualizar
        format.html { redirecionamento(@banner) }
        format.xml  { head :ok }
      else
        erro_atualizar
        format.html { render :action => "editar" }
        format.xml  { render :xml => @banner.errors, :status => :unprocessable_entity }
      end
    end
  end
  
  def apagar
    @banner = Banner.find(params[:id])
    @banner.destroy

    pesquisar
  end

  def pesquisar
    params[:page] ||= 1
    params[:banner] ||= {}
    @banners = Banner::pesquisa_admin(params[:banner],params[:page])
    respond_to do |formato|
      formato.html { render :action => "index" }
      formato.js   { render :action => "pesquisar.rjs" }
      formato.xml  { render :xml => @banners }
    end
  end

end
