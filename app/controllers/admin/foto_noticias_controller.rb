class Admin::FotoNoticiasController < Admin::RestritoController
  def index
    pesquisar
  end

  def novo
    @foto_noticia = FotoNoticia.new
    
    respond_to do |format|
      format.html # new.html.erb
      format.xml  { render :xml => @foto_noticia }
    end
  end

  def editar
    @foto_noticia = FotoNoticia.find(params[:id])
  end

  def criar
    return redirect_to(:action => :index) if acao_cancelar?
    
    @foto_noticia = FotoNoticia.new params[:foto_noticia]

    respond_to do |format|
      if @foto_noticia.save
        sucesso_criar
        format.html { redirecionamento(@foto_noticia) }
        format.xml  { render :xml => @foto_noticia, :status => :created, :location => @foto_noticia }
      else
        erro_criar
        format.html { render :action => "novo" }
        format.xml  { render :xml => @foto_noticia.errors, :status => :unprocessable_entity }
      end
    end
  end

  def atualizar
    return redirect_to(:action => :index) if acao_cancelar?
    
    @foto_noticia = FotoNoticia.find(params[:id])

    respond_to do |format|
      if @foto_noticia.update_attributes(params[:foto_noticia])
        sucesso_atualizar
        format.html { redirecionamento(@foto_noticia) }
        format.xml  { head :ok }
      else
        erro_atualizar
        format.html { render :action => "editar" }
        format.xml  { render :xml => @foto_noticia.errors, :status => :unprocessable_entity }
      end
    end
  end

  def apagar
    @foto_noticia = FotoNoticia.find(params[:id])
    @foto_noticia.destroy

    pesquisar
  end

  def pesquisar
    params[:page] ||= 1
    params[:foto_noticia] ||= {}
    @foto_noticias = FotoNoticia::pesquisa_admin(params[:foto_noticia],params[:page])
    respond_to do |formato|
      formato.html { render :action => "index" }
      formato.js   { render :action => "pesquisar.rjs" }
      formato.xml  { render :xml => @foto_noticias }
    end
  end
end
